package main

import (
	"io/ioutil"
	"os/exec"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	// To kill the daemon process you must call kill.go in the folder where the reportgtrd.pid file is located
	dat, err := ioutil.ReadFile("./reportgtrd.pid")
	check(err)

	_, cmdErr := exec.Command("sh", "-c", "kill "+string(dat)).Output()

	check(cmdErr)
}
