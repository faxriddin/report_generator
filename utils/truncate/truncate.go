package main

import (
	"log"
	"os"
)

var (
	myfile *os.FileInfo
	e      error
)

func main() {
	// To Truncate the log file you must call Truncate.go in the folder where the reportgtrd.log file is located
	err := os.Truncate("reportgtrd.log", 0)
	if err != nil {
		log.Fatal(err)
	}
}
