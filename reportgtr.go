package main

import (
	"database/sql"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"net"
	"os"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/joho/godotenv"
	"github.com/sevlyar/go-daemon"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

//JRPCParams is method params
type JRPCParams struct {
	ReportID string `json:"reportID"`
}

// JSONRPC is jsonrpc2 request object
type JSONRPC struct {
	ID      int        `json:"id"`
	Jsonrpc string     `json:"jsonrpc"`
	Method  string     `json:"method"`
	Params  JRPCParams `json:"params,omitempty"`
}

var (
	signal = flag.String("s", "",
		`Send signal to the daemon:
    		quit — graceful shutdown
    		stop — fast shutdown
			reload — reloading the configuration file`)
)

const sleepTimeWhenNoOrders = 2

var (
	workersCount      int
	errorConv         error
	debug             bool
	serviceServerHost string
	serviceServerPort string
	serverAddr        string

	pgHost     string
	pgPort     string
	pgDBName   string
	pgUser     string
	pgPassword string
	dsn        string
)

// request constants and variables
const (
	JSONRPCVersion string = "2.0"
	JSONRPCMethod  string = "generateReport"
)

const (
	errorStateTCP int = -1
)

// manager variables
var (
	stop        = make(chan struct{})
	done        = make(chan struct{})
	doneWork    = make(chan int)
	mutex       = &sync.Mutex{}
	freeWorkers = 0
	cntxt       *daemon.Context
)

type logWriter struct {
}

func (writer logWriter) Write(bytes []byte) (int, error) {
	return fmt.Print("[" + time.Now().UTC().Format("2006-01-02 15:04:05.999999") + "] " + string(bytes))
}

//TO DO must be README file
func main() {
	loadConfig()
	initLogger()
	cntxt = initDaemon()
	defer cntxt.Release()
	go manager(workersCount)
	serveSignals()
}

func loadConfig() {
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	workersCount, errorConv = strconv.Atoi(os.Getenv("WORKERS_COUNT"))
	if errorConv != nil {
		log.Fatalf("Error loading WORKERS_COUNT from .env file")
	}

	debug, errorConv = strconv.ParseBool(os.Getenv("ENABLE_LOG"))
	if errorConv != nil {
		log.Fatalf("Error loading DEBUG from .env file")
	}

	// TCP variables
	serviceServerHost = os.Getenv("TCP_HOST")
	if serviceServerHost == "" {
		log.Fatalf("Error loading TCP_HOST from .env file")
	}

	serviceServerPort = os.Getenv("TCP_PORT")
	if serviceServerPort == "" {
		log.Fatalf("Error loading TCP_PORT from .env file")
	}

	serverAddr = serviceServerHost + ":" + serviceServerPort

	// Postgres DBMS variables
	pgHost = os.Getenv("PG_HOST")
	if pgHost == "" {
		log.Fatalf("Error loading PG_HOST from .env file")
	}

	pgPort = os.Getenv("PG_PORT")
	if pgPort == "" {
		log.Fatalf("Error loading PG_PORT from .env file")
	}

	pgDBName = os.Getenv("PG_DB_NAME")
	if pgDBName == "" {
		log.Fatalf("Error loading pgDBName from .env file")
	}

	pgUser = os.Getenv("PG_USER")
	if pgUser == "" {
		log.Fatalf("Error loading PG_USER from .env file")
	}

	pgPassword = os.Getenv("PG_PASSWORD")
	if pgPassword == "" {
		log.Fatalf("Error loading PG_PASSWORD from .env file")
	}

	dsn = `host=` + pgHost +
		` port=` + pgPort +
		` dbname=` + pgDBName +
		` user=` + pgUser +
		` password=` + pgPassword +
		` sslmode=disable TimeZone=Asia/Tashkent`

	if workersCount < 1 {
		log.Fatalf(`Error: reportgtrd is not started! 
				Workers count is lass than 1`)
		return
	}
}

func initLogger() {
	//Disable default prefix of log library
	log.SetFlags(0)
	log.SetOutput(new(logWriter))
}

func initDaemon() *daemon.Context {
	flag.Parse()
	daemon.AddCommand(
		daemon.StringFlag(signal, "quit"),
		syscall.SIGQUIT, termHandler)
	daemon.AddCommand(
		daemon.StringFlag(signal, "stop"),
		syscall.SIGTERM, termHandler)
	daemon.AddCommand(
		daemon.StringFlag(signal, "reload"),
		syscall.SIGHUP, reloadHandler)

	cntxt = &daemon.Context{
		PidFileName: "reportgtrd.pid",
		PidFilePerm: 0644,
		LogFileName: "reportgtrd.log",
		LogFilePerm: 0640,
		WorkDir:     "./",
		Umask:       027,
	}

	if len(daemon.ActiveFlags()) > 0 {

		reportgtrd, err := cntxt.Search()
		if err != nil {
			log.Printf("Error: Unable send signal to the reportgtrd: %s", err.Error())
		}

		daemon.SendCommands(reportgtrd)
		os.Exit(1)
	}

	child, err := cntxt.Reborn()
	if err != nil {
		log.Fatalf("Error on Reborn: %s", err.Error())
	}

	if child != nil {
		//ParentPost daemon successfully run
		os.Exit(0)
	}

	log.Println("- - - - - - - - - - - - - - -")
	log.Println("reportgtrd successfull started")
	return cntxt
}

func manager(workersCount int) {
	mutex = &sync.Mutex{}
	freeWorkers = workersCount

LOOP:
	for {
		mutex.Lock()
		select {
		case <-doneWork:
			freeWorkers = freeWorkers + 1
		case <-stop:
			break LOOP
		default:
		}
		mutex.Unlock()

		//Checking free workers to call to work
		if freeWorkers > 0 && checkTCPConnection() {
			if reportOrderID := getOrder(); reportOrderID != "" {
				go worker(workersCount-freeWorkers, reportOrderID)
				mutex.Lock()
				freeWorkers = freeWorkers - 1
				mutex.Unlock()
			} else {
				sleepDaemonIf(sleepTimeWhenNoOrders)
			}
		}
	}

	done <- struct{}{}
}

func serveSignals() {
	err := daemon.ServeSignals()
	if err != nil {
		log.Printf("Error: %s", err.Error())
	}
	log.Println("reportgtrd terminated")
}

func worker(workerID int, reportOrderID string) {
	var msg string

	conn, connSuccess := tcpConn(reportOrderID)

	if connSuccess != true {
		return
	}
	defer conn.Close()

	requestID := (rand.New(rand.NewSource(time.Now().UnixNano()))).Intn(100)

	// reqObj := `{"jsonrpc":"2.0","method":"generateReport", "params":{"reportID":"5fe18f9c4eacb40fffe1970b"}, "id":1}`
	reqObj := JSONRPC{
		ID:      requestID,
		Jsonrpc: JSONRPCVersion,
		Method:  JSONRPCMethod,
		Params: JRPCParams{
			ReportID: reportOrderID,
		},
	}

	// fmt.Printf("contents of decoded json is: %#v\r\n", reqObj)
	var jsRPC []byte
	jsRPC, err := json.Marshal(&reqObj)
	if err != nil {
		msg = "Write to json failed, jsRPC: " + string(jsRPC[:]) + ", err: " + err.Error()
		mutexLog(msg)
		updateOrderState(reportOrderID, -3, false, msg)
		doneWork <- errorStateTCP
		return
	}

	_, errJSRPC := conn.Write(jsRPC)
	if errJSRPC != nil {
		msg = "Write to server failed: " + string(jsRPC[:]) + ", err: " + errJSRPC.Error()
		updateOrderState(reportOrderID, -3, false, msg)
		doneWork <- errorStateTCP
		return
	}

	reply := make([]byte, 1024)

	_, errReply := conn.Read(reply)
	if errReply != nil {
		msg = "Read from server failed: " + errReply.Error()
		mutexLog(msg)
		updateOrderState(reportOrderID, -3, false, msg)
		doneWork <- errorStateTCP
		return
	}

	doneWork <- workerID
}

func tcpConn(reportOrderID string) (*net.TCPConn, bool) {
	var msg string
	tcpServerAddr, errResolveTCPAddr := net.ResolveTCPAddr("tcp", serverAddr)
	if errResolveTCPAddr != nil {
		msg = "ResolveTCPAddr failed:" + errResolveTCPAddr.Error()
		mutexLog(msg)
		updateOrderState(reportOrderID, 0, true, msg)
		doneWork <- errorStateTCP
		return nil, false
	}

	conn, errDialTCP := net.DialTCP("tcp", nil, tcpServerAddr)
	if errDialTCP != nil {
		msg = "Dial failed:" + errDialTCP.Error()
		mutexLog(msg)
		updateOrderState(reportOrderID, 0, true, msg)
		doneWork <- errorStateTCP
		return nil, false
	}

	return conn, true
}

func termHandler(sig os.Signal) error {
	log.Println("terminating...")
	stop <- struct{}{}

	if sig == syscall.SIGQUIT {
		<-done
	}

	return daemon.ErrStop
}

func reloadHandler(sig os.Signal) error {
	loadConfig()
	mutexLog("configuration reloaded")
	return nil
}

func connectToDB() (*gorm.DB, *sql.DB) {
	db, errOpen := gorm.Open(
		postgres.New(
			postgres.Config{
				DSN: dsn,
				// disables implicit prepared statement usage
				PreferSimpleProtocol: true,
			}), &gorm.Config{},
	)

	if errOpen != nil {
		log.Printf("Error: %s", errOpen.Error())
	}

	pgDb, errDB := db.DB()
	if errDB != nil {
		log.Printf("Error: %s", errDB.Error())
	}

	return db, pgDb
}

func getOrder() string {

	db, pgDb := connectToDB()

	var reportOrderID string = ""
	db.Raw(`select id 
			from order_by_report 
			where ex_state = 0 
			order by created_at 
			limit 1`).Row().Scan(&reportOrderID)

	if reportOrderID != "" {
		db.Exec(`
			UPDATE order_by_report 
			SET make_start_at=?, ex_state=? 
			WHERE id =?`, time.Now(), 2, reportOrderID)
	}

	defer pgDb.Close()
	return reportOrderID
}

// Update state value
func updateOrderState(reportOrderID string, state int, reset bool, errMsg string) {
	db, pgDb := connectToDB()
	defer pgDb.Close()

	var result *gorm.DB
	if reset == true {
		result = db.Exec(`
		UPDATE order_by_report 
		SET make_start_at=?, ex_state=?, error_msg=? 
		WHERE id =?`, nil, state, errMsg, reportOrderID)

		if result.RowsAffected > 0 {
			return
		}

		log.Println("Err: update (reset) record (reportOrderID): " + reportOrderID + " state: " + strconv.Itoa(state) + " errMsg:" + errMsg)
		return
	}

	result = db.Exec(`
		UPDATE order_by_report 
		SET make_end_at=?, ex_state=?, error_msg=?  
		WHERE id =?`, time.Now(), state, errMsg, reportOrderID)

	if result.RowsAffected > 0 {
		return
	}

	log.Println("Err: update record (reportOrderID): " + reportOrderID + " state: " + strconv.Itoa(state) + " errMsg:" + errMsg)
	return
}

func checkTCPConnection() bool {
	tcpServerAddr, errResolveTCPAddr := net.ResolveTCPAddr("tcp", serverAddr)
	if errResolveTCPAddr != nil {
		mutexLog("ResolveTCPAddr failed:" + errResolveTCPAddr.Error())
		sleepDaemonIf(10)
		return false
	}

	conn, errDialTCP := net.DialTCP("tcp", nil, tcpServerAddr)
	if errDialTCP != nil {
		mutexLog("Dial failed:" + errDialTCP.Error())
		sleepDaemonIf(10)
		return false
	}
	defer conn.Close()
	return true
}

func sleepDaemonIf(sleepSeconds time.Duration) {
	mutex.Lock()
	if freeWorkers == workersCount {
		time.Sleep(sleepSeconds * time.Second)
	}
	mutex.Unlock()
}

func mutexLog(msg string) {
	mutex.Lock()
	log.Println(msg)
	mutex.Unlock()
}
