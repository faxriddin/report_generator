# Report Generator Daemon  

Daemon that call the report generator API service to generate report

```code  

IMG: SCHEME of Daemon work process

```  

## Environment  

 **To start the reportgtrd daemon, you must create a .env file and enter all the variables that are in the default.env file with the correct values.**  

*Example values for .env variables, to define all variables, see the file **default.env***  

```env
ENABLE_LOG=false
WORKERS_COUNT=5
TCP_HOST=127.0.0.1
TCP_PORT=1207
PG_HOST=localhost
PG_PORT=5432
PG_DB_NAME=db_name
PG_USER=db_user
PG_PASSWORD=db_password
```

## Required  

- workers count must be more than 0  
- disable ENABLE_LOG for production version, and use only for debugging time, log file name is *reportgtrd.log*. All errors, regardless of the value of the variable, will be logged
- the values of the TCP_ * variables must be the same as on the TCP server (jsonrpc2 TCP server configuration)
- the daemon uses the order_by_report relation, so it should be  

## Notes  

- Default sleep duration time when no orders is equal to *2* second  

## Utils  

Tools for developer that help to kill process and clear log file.  
*To use the tools, you need to call their from the folder where reportgtrd.[pid | log] files is located*  

### kill  

To kill reportgtrd daemon

```code
go run ./utils/kill/kill.go
```  

### truncate

To truncate reoportgtrd.log file  

```code
go run ./utils/truncate/truncate.go
```  
